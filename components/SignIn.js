import React from 'react';
import { StyleSheet, Text, View, TextInput, Button } from 'react-native';


const SignIn = props => {
    return (
        <View style={styles.container}>
            <View style={styles.email}>
                <Text>Email</Text>
                <TextInput placeholder ='email address'
                style = {styles.emailText}></TextInput>
            </View>

            <View style={styles.password}>
            <Text>Password</Text>
                <TextInput placeholder =''
                style = {styles.emailText}
                secureTextEntry = {true}></TextInput>
            </View>

            <View style={styles.signInbuttonCon}>
              <Button title="Sign-in" 
              style = {styles.signinButton}
              />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      //alignItems: 'center',
      justifyContent: 'center',
    },
    email: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      padding: 20,
      },
      password: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      padding: 20,
      },
    emailText:{
      width: "80%",
      borderColor:'blue',
      borderWidth:1,
      padding:10,
    },
    passwordText: {
      width: "80%",
      borderColor:'blue',
      borderWidth:1,
      padding:10,
    },
    signInbuttonCon:{
      alignContent: 'flex-end',
    },
    signinButton: {
      width: 100,
    },
  });

export default SignIn